#!/bin/bash
set -u;
declare -r SCRIPT_PATH=`realpath $0`;        # chemin  absolu complet du script rep + nom
declare -r SCRIPT_REP=`dirname $SCRIPT_PATH`; # repertoire absolu du script (pas de slash de fin)
declare -r SCRIPT_FILENAME=${0##*/};         # nom.ext
declare -r SCRIPT_NAME=${SCRIPT_FILENAME%.*}   # uniquement le nom
declare -r SCRIPT_EXT="sh"                   # uniquement l'extention
VERSION="v0.0.4";
#$PWD;                            # repertoire de travail
#$?: retour de la derniere commande shell


################
# colorisation #
################
RESET_COLOR=$(tput sgr0)
BOLD=`tput smso`
NOBOLD=`tput rmso`

BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
CYAN=`tput setaf 4`
MAGENTA=`tput setaf 5`
BLUE=`tput setaf 6`
WHITE=`tput setaf 7`

NORMAL=$WHITE
INFO=$BLUE
CMD=$YELLOW
WARN=$RED
TITRE1=$GREEN
TITRE2=$MAGENTA
DEBUG_COLOR=$MAGENTA


####################
# codes de sorties #
####################
declare -r E_ARG_NONE=65
declare -r E_ARG_BAD=66 #perso


#############
# aide bash #
#############
#est ce que $sequence_lettres est dans "$mot" ?
#if echo "$mot" | grep -q "$sequence_lettres"
# L'option "-q" de grep supprime l'affichage du résultat.
#then
#  echo "$sequence_lettres trouvée dans $mot"
#else
#  echo "$sequence_lettres non trouvée dans $mot"
#fi

#############
# variables #
#    init   #
#############

# --- Definir si root ou user --- #
[ "$(id -u)" == "0" ] && declare -r IS_ROOT=1 || declare -r IS_ROOT=0;

# --- variables des includes --- #
declare -r UPDATE_HTTP="http://updates.legite.org/legite-install";

# --- variables generiques --- #
argNb=$#;
isUpdate=0;                     # demande de update
verbose=0;
isDebug=0;
isDebugShowVars=0;              # montrer les variables

# --- variables des parametres supplementaires --- #


# --- variables generiques --- #
sortie=0; # demande de sortie du programme

# --- variables du projet --- #
isCpConf=0      # --cpConf
isCreateProjet=0 #--createProjet
isLink=0;       # --link

isMin=0;        # --min
isMinCSS=0;     # --minCSS
isMinJS=0;      # --minJS

isCleanAll=0;   # --cleanAll
isCleanCSS=0;   # --cleanCSS
isCleanJS=0;    # --cleanJS

isStatic=0;		# --static
isRemote=0;		# --remote
islocalVersionLoad=0
outFileCSS='styles';
outFileJS='scripts';


# #################### #
# functions generiques #
# #################### #
usage (){
    if [ $argNb -eq 0 ];
    then
        echo "$SCRIPT_FILENAME travail dans le repertoire local. Verifier votre localisation!"
        #echo " [-d] [--showVars] # fonctions de debug";
        #echo " --test ";
        echo " --cpConf, --clean --cleanJS --cleanCSS  --link --min --minJS --minCSS"
        exit $E_ARG_NONE;
    fi
}

showVars(){
if [ $isDebugShowVars -eq 1 ];
    then
        isDebugOld=$isDebug;
        isDebug=1;
        #showDebug " $LINENO"
        showDebug " argNb=$argNb"
        showDebug " IS_ROOT=$IS_ROOT"
        showDebug " isUpdate=$isUpdate"
        showDebug " verbose=$verbose"
        showDebug " isDebug=$isDebugOld"
        showDebug " isDebugShowVars=$isDebugShowVars"
        
        showDebug " isTest=$isTest"

        showDebug " uri=$uri"

        isDebug=$isDebugOld;
    fi
}


#########
# DEBUG #
#########
#evalCmd($cmd $ligneNu $txt)
evalCmd(){
    local ligneNu="";   if [ ! -z ${2+x} ]; then ligneNu="[$2]:"; fi
    local txt="";       if [ ! -z ${3+x} ]; then txt="$3"; fi

    echo "${DEBUG_COLOR}$ligneNu$CMD$1$NORMAL$txt";
    eval "$1";
    return $?
}


#showDebug(texte)
showDebug(){
if [ $isDebug -eq 1 ];
then
    #ligneNo=$1;
    texte="$1";
    if [ $# -eq 2 ]; then
        texte=$2;
    fi
    echo "${DEBUG_COLOR}$texte$NORMAL"
fi
}


# - fonction executer a chaque sortie du programme - #
# $1: code retour a transmettre
sortie (){
    echo "sortie"
    displayVars
    exit $1
}


# #################### #
# functions du projet  #
# #################### #
# - copie le fichier de configuration par defaut dans le rep local - #
cpConf(){
	if [ $isCpConf -eq 1 ];then
        if [ -x "$PWD/managesite.conf.sh" ];then
            echo "$WARN un fichier $INFO $PWD/managesite.conf.sh $WARN existe deja.$NORMAL"
            return
        fi
    echo "Copie du fichier $scriptRep/managesite.conf.sh"
    evalCmd "cp --no-clobber $scriptRep/managesite.conf.sh $PWD/managesite.conf.sh";
    fi
}


linker(){
    source="$1";
    dest="$2"

    # source existe PAS -> on sort
    if [ ! -e "$source" ];then
        echo "$WARN la source $source existe pas!$NORMAL";
        return
    fi

    # dest existe -> on sort->on la unlink
    if [ -e "$dest" ];then
		echo "$NORMAL la destination $dest existe $NORMAL";
		#evalCmd "unlink $dest";### JAMAIS CA!!!
        return
    fi
    evalCmd "link $source $dest";
}

lier(){
    source="$1";
    dest="$2"

    # source existe PAS -> on sort
    if [ ! -e "$source" ];then
        echo "$WARN la source $source existe pas!$NORMAL";
        return
    fi

    # dest existe -> on sort
    if [ -e "$dest" ];then
        echo "$NORMAL la destination $dest existe $NORMAL";
        return
    fi
    evalCmd "ln -s $source $dest";
}


# ----------------------------------------------- #
# - functions utiliser par managesite.conf.sh() - #
# ----------------------------------------------- #
# - concatenation de fichier - #
concateCSS(){
	echo ""              >> ./web/locales/$outFileCSS.css
	echo "/*! - $1 - */" >> ./web/locales/$outFileCSS.css
    cat "$1"             >> ./web/locales/$outFileCSS.css
}

concateJS(){
    echo ""              >> ./web/locales/$outFileJS.js
	echo "/*! - $1 - */" >> ./web/locales/$outFileJS.js
    cat "$1"             >> ./web/locales/$outFileJS.js
}


# ----------------------------------------------- #
# - functions de nettoyage) - #
# ----------------------------------------------- #
cleanAll(){
	if [ $isCleanAll -eq 1 ];then
        echo "$INFO Activation du nettoyage de tous les fichiers $NORMAL";

        rm -R ./vendors

        isCleanCSS=1;
        isCleanJS=1;
    fi
}

cleanCommun(){
    #elements a supprimer dans troutes les situations
    echo "";
}


cleanCSS(){
	if [ $isCleanCSS -eq 1 ];then
        echo "$INFO Nettoyage des fichiers CSS $NORMAL";
        unlink ./web/css-vendors

        rm -R ./web/locales/*.css
    fi
}

cleanJS(){
	if [ $isCleanJS -eq 1 ];then
        echo "$INFO Nettoyage des fichiers JS $NORMAL";
        rm -R ./web/js-vendors
        rm -R ./web/locales/*.js
    fi
    
}

# ----------------------------------------------------------------- #
# - creer une copie d'un js|css en ajoutant la version en suffixe - #
# %1:fn:fichier nom
# %2:fe: fichier extention (sans le point)linkFiles
# (calc)fv: nom du fichier avec la version en suffixe
# utilisser par localSave()                                         #
# ----------------------------------------------------------------- #
versionSave() {
	fn=$1;
	fe=".$2";	# ajout du point 
        fv="$fn-$tag";
	echo "creation de la copie $fn$fe vers ./versions/$fv$fe";
        cp $fn$fe ./versions/$fv$fe
	if [ "$fe" = "sh" ];then
		echo "le fichier est un script -> +x"
		chmod +x ./versions/$fv$fe
	fi
}


# ----------------------------------------------------- #
# - generere les fichiers contennue dans ./locales/ ) - #
# - generation de script.js et styles.css -             #
# ----------------------------------------------------- #
# - minimise CSS - #
minimiseCSS(){
	if [ $isMinCSS -eq 1 ];then
	if [ $islocalVersionLoad -eq 1 ];then
        evalCmd 'mkdir -p web/locales'

        # -- creation du fichier concatenné -- #
        evalCmd "touch ./web/locales/$outFileCSS.css"
        echo '@charset "utf-8";' > ./web/locales/$outFileCSS.css; 

   		# -- concatenation des fichiers css -- #
	    local_concateCSS;
        evalCmd "ls -l web/locales/$outFileCSS.css"
        
   		# -- minimisation des fichiers css -- #
		# --- yuicompressor NE rajoute PAS automatiquement l'extention ".js|.css" --- #
        evalCmd "java -jar ${SCRIPT_REP}/yuicompressor-2.4.8.jar ./web/locales/$outFileCSS.css -o ./web/locales/$outFileCSS.min.css"
        evalCmd "ls -l web/locales/$outFileCSS.min.css"

    fi #if [ $islocalVersionLoad -eq 1 ]then
	fi #if [ $isMin -eq 1 ] then
}


# - minimise JS - #
minimiseJS(){
	if [ $isMinJS -eq 1 ];then
	if [ $islocalVersionLoad -eq 1 ];then
        evalCmd 'mkdir -p web/locales'

        # -- creation du fichier concatenné -- #
        evalCmd "touch ./web/locales/scripts.js"
        echo '/*! scripts.js */' > ./web/locales/$outFileJS.js; 

   		# -- concatenation des fichiers js -- #
	    local_concateJS;
        evalCmd "ls -l web/locales/$outFileJS.js"
        
   		# -- minimisation des fichiers js -- #
		# --- yuicompressor NE rajoute PAS automatiquement l'extention ".js|.css" --- #
        evalCmd "java -jar ${SCRIPT_REP}/yuicompressor-2.4.8.jar ./web/locales/$outFileJS.js -o ./web/locales/$outFileJS.min.js"
        evalCmd "ls -l web/locales/$outFileJS.min.js"

    
    fi #if [ $islocalVersionLoad -eq 1 ]then
	fi #if [ $isMin -eq 1 ] then
}


# --------------------------------------------- #
# callStatification                             #
# appel localStatification dans localVersion.sh #
# --------------------------------------------- #
callStatification (){
	if [ $isStatic -eq 1 ];then
		echo "";
		echo "$couleurINFO # - gitVersion.sh - callStatification - #$couleurNORMAL";
		statification;
		echo "$couleurINFO Etape suivante: ./scripts/gitVersion.sh --remote  $couleurNORMAL";
	fi
}

# --------------------------------- #
# SyncRemote                        #
# appel remote dans localVersion.sh #
# --------------------------------- #
callSyncRemote(){
	if [ $isRemote -eq 1 ];then
		echo "$couleurINFO # - n.sh:syncRemote() - #$couleurNORMAL";
		#echo "$couleurINFO # - Penser a mettre a jours la version statique avec ./scripts/gitVersion.sh --static - #$couleurNORMAL";
		echo "$couleurINFO # - http://doc.ubuntu-fr.org/lftp - #$couleurNORMAL";
		echo "$couleurINFO # - configurer ~.netrc pour ne pas a avoir a fournir le passsword - #$couleurNORMAL";
		syncRemote;
	fi
}


##################
# analyse du psp #
##################
#$0	Contient le nom du script tel qu'il a été invoqué
#$*	L'ensembles des paramètres sous la forme d'un seul argument
#$@	L'ensemble des arguments, un argument par paramètre
#$#	Le nombre de paramètres passés au script
#$?	Le code retour de la dernière commande
#$$	Le PID su shell qui exécute le script
#$!	Le PID du dernier processus lancé en arrière-plan


# ########## #
# parametres #
# ########## #
TEMP=`getopt \
     --options v::dVhtiulmsr \
     --long verbose::,help,debug,version,cpConf,link,cleanAll,clean,cleanCSS,cleanJS,min,minCSS,minJS \
     -- "$@"`


# Note the quotes around `$TEMP': they are essential!
eval set -- "$TEMP"
#if [ $? -eq 0 ] ; then echo "options requise. Sortie" >&2 ; exit 1 ; fi
#echo "$couleurWARN nb: $# $couleurNORMAL";
#echo "${INFO}argNb:$NORMAL $#"
#echo "${INFO}arg:$NORMAL $?"

while true ; do
    case "$1" in

        # - fonctions generiques - #
        -h|--help) usage; exit 0; shift ;;
        -V|--version) echo "$VERSION"; exit 0; shift ;;
        -v|--verbose)
            case "$2" in
                "") verbosity=1; shift 2 ;;
                *)  #echo "Option c, argument \`$2'" ;
                verbosity=$2; shift 2;;
            esac ;;

         # - Debug - #
        -d|--debug)
            isDebug=1;
            isDebugShowVars=1;
            shift
            ;;
        --showVars)isDebugShowVars=1;   shift ;;

        # - Mise a jours - #
        # --update)  isUpdate=1;          shift ;;

        # - Librairies - #
        #--showLibs) isShowLibs=1;         shift ;;


        # - parametres liés au projet - #

        # - - #
        --cpConf)
            isCpConf=1;
            shift ;;

		# - createProjet - #
		--createProjet | --createSite)
	        isCreateProjet=1;
    	    shift ;;

        # - lier/linker - #
        -l|--link)
            isLink=1;
            shift ;;


        # - nettoyage preventif - #
        --cleanCSS)
            isCleanCSS=1;
            shift ;;

        --clean|cleanAll)
            isCleanAll=1;
            shift ;;

        --cleanCSS)
            isCleanCSS=1;
            shift ;;

        --cleanJS)
            isCleanJS=1;
            shift ;;

        # - minimiser - #

        -m|--min)
            isMin=1;
            shift ;;

        -m|--minCSS)
            isMinCSS=1;
            shift ;;

        -m|--minJS)
            isMinJS=1;
            shift ;;

        -s|--static)
            shift ;;

        -r|--remote)
            shift ;;


        # - autres - #
        --)
            if [ -z ${2+x} ];
            then
                uri="";
            else
                uri="$2";
            fi
            shift ;
            break ;;

        *)
            echo "option $1 $2 non reconnu"; shift;
            exit 1 ;; # sans exit: si la derniere option est inconnu -> boucle sans fin
        esac
done


########
# main #
########
#clear;
#echo "$INFO####";
#echo "${INFO}SCRIPT_REP:$NORMAL $SCRIPT_REP";
echo "${INFO}$SCRIPT_PATH $VERSION$NORMAL";
#echo "${INFO}SCRIPT_FILENAME:$NORMAL $SCRIPT_FILENAME";
#echo "${INFO}SCRIPT_NAME:$NORMAL $SCRIPT_NAME";

# - 0- usage - #
usage;

# - 1- mise a jours du programme - #
#selfUpdate;

# - Affichage des variables - #
showVars;

# - sortie en cas d'install/mise a jours - #
if [ $isUpdate -eq 1 ]; then
    exit 0;
fi


###########################
# - execution du projet - #
###########################

# - copie du fichier de conf par defaut(si option activée) - #
cpConf

# - chargement de managesite.conf.sh- #
confPath="$PWD/managesite.conf.sh"
if [ -x $confPath ];then
    islocalVersionLoad=1 
    evalCmd ". $confPath"
else
    echo $WARN Pas de fichier $INFO $confPath $NORMAL

fi


# - creation du projet/site- #
#if [ $isCreateProjet -eq 1 ]; then
#	echo "Creation du projet"
#    createProjet;
##else
##    evalCmd "cd sid"
#fi


# - nettoyage preventif - #
cleanAll
cleanCSS
cleanJS
#evalCmd "cleanCommun";


# - lier/linker - #
if [ $isLink -eq 1 ]; then
    linkFiles;
fi


# - minimiser - #
if [ $isMin -eq 1 ]; then
    isMinCSS=1;
    isMinJS=1;
fi

minimiseCSS
minimiseJS
